import domain.filemanagement.FileType;
import domain.projectmangement.BacklogItem;
import domain.projectmangement.backlogitemstate.BacklogItemCreatedState;
import domain.projectmangement.backlogitemstate.BacklogItemDoingState;
import domain.projectmangement.backlogitemstate.BacklogItemState;
import domain.projectmangement.backlogitemstate.BacklogItemTodoState;
import domain.user.User;
import domain.user.UserRoleStudent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static domain.filemanagement.FileType.FILE;
import static domain.filemanagement.FileType.FOLDER;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

import domain.filemanagement.File;

import java.util.ArrayList;
import java.util.List;

@TestInstance(PER_CLASS)
public class BacklogItemTest {

    @Test
    public void createItem(){
        User usr = new User("student", new UserRoleStudent());
        BacklogItem item = new BacklogItem("item", "desc", usr);

        Assertions.assertNotNull(item);

    }

    @Test
    public void GetSetItemTitle(){
        User usr = new User("student", new UserRoleStudent());
        BacklogItem item = new BacklogItem("item", "desc", usr);

        item.setTitle("title");

        Assertions.assertEquals("title", item.getTitle());

    }

    @Test
    public void GetSetItemDesc(){
        User usr = new User("student", new UserRoleStudent());
        BacklogItem item = new BacklogItem("item", "desc", usr);

        item.setDescription("title");

        Assertions.assertEquals("title", item.getDescription());

    }

    @Test
    public void GetSetItemUser(){
        User usr = new User("student", new UserRoleStudent());
        User usr2 = new User("student2", new UserRoleStudent());
        BacklogItem item = new BacklogItem("item", "desc", usr);

        item.setAssignedUser(usr2);

        Assertions.assertEquals(usr2, item.getAssignedUser());

    }

    @Test
    public void GetSetSetItemState(){
        User usr = new User("student", new UserRoleStudent());

        BacklogItem item = new BacklogItem("item", "desc", usr);

        BacklogItemState state = new BacklogItemDoingState();

        item.setState(state);

        Assertions.assertEquals(state, item.getState());

    }

    @Test
    public void ItemStateNext(){
        User usr = new User("student", new UserRoleStudent());

        BacklogItem item = new BacklogItem("item", "desc", usr);

        item.next();

        BacklogItemState state = new BacklogItemTodoState();

        boolean cstate = false;

        if (item.getState().toString().contains("Todo")){
            cstate = true;
        }


        Assertions.assertTrue(cstate);

    }

    @Test
    public void ItemStatePrevious(){
        User usr = new User("student", new UserRoleStudent());

        BacklogItem item = new BacklogItem("item", "desc", usr);
        BacklogItemState state = new BacklogItemDoingState();
        item.setState(state);

        item.previous();

        boolean cstate = false;

        if (item.getState().toString().contains("Todo")){
            cstate = true;
        }

        Assertions.assertTrue(cstate);
    }

    @Test
    public void ItemStateTodoPreviousThrowsError(){
        User usr = new User("student", new UserRoleStudent());

        BacklogItem item = new BacklogItem("item", "desc", usr);
        BacklogItemState state = new BacklogItemTodoState();
        item.setState(state);

        //item.previous();
        Exception a = null;
        try {
            item.previous();
        }catch (Exception e){
            a = e;
        }

        Assertions.assertNotNull(a);

    }


}
