import domain.filemanagement.FileType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static domain.filemanagement.FileType.FILE;
import static domain.filemanagement.FileType.FOLDER;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

import domain.filemanagement.File;

import java.util.ArrayList;
import java.util.List;

@TestInstance(PER_CLASS)
public class FileTest {

    @Test
    public void canCreateFile(){

        File file = new File(FILE, "test.json", "0101010");

        Assertions.assertEquals(FILE, file.getFileType());
    }

    @Test
    public void canGetSetContentOfFile(){

        File file = new File(FILE, "test.json", "0101010");

        String scont = "0x01";

        file.setContent(scont);

       String cont =  file.getContent();

        Assertions.assertEquals(cont, scont);
    }

    @Test
    public void canGetSetCommentsOfFile(){

        File file = new File(FILE, "test.json", "0101010");

        String scont = "0x01";

        List comments = new ArrayList();

        comments.add(scont);


        file.setComments(comments);

        List<String> comments2 =  file.getComments();

        String comment2 = comments2.get(0);

        Assertions.assertEquals(comment2, scont);
    }


    @Test
    public void getSetName(){

        File file = new File(FILE, "test.json", "0101010");

        file.setName("a.exe");

        Assertions.assertEquals("a.exe", file.getName());
    }

    @Test
    public void setFileType(){

        File file = new File(FILE, "test.json", "0101010");

        file.setFileType(FOLDER);

        Assertions.assertEquals(FOLDER, file.getFileType());
    }


    @Test
    public void canCreateFolder(){

        //File file2 = new File(FILE, "file", "0101010");

        File folder = new File(FOLDER, "folder", "");

        Assertions.assertEquals(FOLDER, folder.getFileType());
    }

    @Test
    public void canAddGetFolderFile(){

        File file = new File(FILE, "test.json", "0101010");

        File folder = new File(FOLDER, "folder", "");

        folder.addFile(file);

        List<File> files = folder.getFiles();

        File getFile = files.get(0);

        Assertions.assertEquals(file, getFile);
    }

    @Test
    public void canRemoveFolderFile(){

        File file = new File(FILE, "test.json", "0101010");

        File folder = new File(FOLDER, "folder", "");

        folder.addFile(file);

        folder.removeFile(file);

        List<File> files = folder.getFiles();

        List<File> emptyfiles = new ArrayList<>();

        Assertions.assertEquals(files, emptyfiles);
    }


    @Test
    public void canAddListOfFiles(){

        File file = new File(FILE, "test.json", "0101010");
        File file2 = new File(FILE, "test.json", "0101010");

        File folder = new File(FOLDER, "folder", "");


        List<File> files = new ArrayList<>();

        files.add(file);
        files.add(file2);

        folder.setFiles(files);

        List<File> files2 = folder.getFiles();

        File getFile = files2.get(0);


        Assertions.assertEquals(file, getFile);
    }


}
