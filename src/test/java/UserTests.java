import domain.user.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import domain.forum.Forum;
import domain.forum.Thread;
import domain.forum.Post;

import domain.projectmangement.BacklogItem;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)


public class UserTests {

    private UserRoleFactory productOwnerFactory;
    private UserRoleFactory scrumMasterFactory;
    private UserRoleFactory studentFactory;

    @BeforeAll
    void initAll() {
        productOwnerFactory = new UserRoleProductOwnerFactory();
        scrumMasterFactory = new UserRoleScrummasterFactory();
        studentFactory = new UserRoleStudentFactory();
    }

    @Test
    void UserCanCreate() {
        User usr = new User("student", productOwnerFactory.createUserRole());
        Assertions.assertNotNull(usr);
    }

    @Test
    void UserCanGetName() {
        User usr = new User("student", scrumMasterFactory.createUserRole());

        String aname = usr.getUsername();

        Assertions.assertEquals("student", aname);
    }

    @Test
    void UserCanSetName() {
        User usr = new User("student", studentFactory.createUserRole());

        usr.setUsername("test");

        String aname = usr.getUsername();

        Assertions.assertEquals("test", aname);
    }

    @Test
    void UserCanGetRole() {

        UserRole std = new UserRoleStudent();

        User usr = new User("student", std);

        Assertions.assertEquals(usr.getUserRole(), std);
    }

    @Test
    void UserCanSetRole() {

        UserRole std = new UserRoleStudent();

        User usr = new User("student", new UserRoleScrummaster());

        usr.setUserRole(std);

        Assertions.assertEquals(usr.getUserRole(), std);
    }

    @Test
    void UserRoleKnowsType() {
        Assertions.assertEquals("Product Owner", productOwnerFactory.createUserRole().whoAmI());
        Assertions.assertEquals("Scrum Master", scrumMasterFactory.createUserRole().whoAmI());
        Assertions.assertEquals("Student", studentFactory.createUserRole().whoAmI());
    }
}
