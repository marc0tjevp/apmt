import domain.filemanagement.File;
import domain.filemanagement.Repository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import domain.filemanagement.Branch;

import java.util.ArrayList;
import java.util.List;

import static domain.filemanagement.FileType.FILE;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class RepositoryTest {

    @Test
    void CreateRepository() {

        Repository repo = new Repository();

        Assertions.assertNotNull(repo);
    }

    @Test
    void RepoAddBranch() {

        Repository repo = new Repository();

        Branch branch = new Branch("test");

        repo.addBranch(branch);

        List<Branch> brs = repo.getBranches();

        Branch main = brs.get(0);

        Assertions.assertEquals(main, branch);
    }

    @Test
    void RepoRemoveBranch() {

        Repository repo = new Repository();

        Branch branch = new Branch("test");

        repo.addBranch(branch);

        repo.removeBranch(branch);

        List<Branch> brs = repo.getBranches();

        List<Branch> brs2 = new ArrayList<>();

        Assertions.assertEquals(brs, brs2);
    }


}

