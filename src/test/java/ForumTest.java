import domain.forum.Post;
import domain.projectmangement.BacklogItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import domain.forum.Forum;
import domain.forum.Thread;
import domain.forum.Post;

import domain.user.User;
import domain.user.UserRole;
import domain.user.UserRoleStudent;
import domain.user.UserRoleProductOwner;
import domain.user.UserRoleScrummaster;

import domain.projectmangement.BacklogItem;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class ForumTest {

    @Test
    void  ForumCanCreate() {
        Forum forum = new Forum();
        Assertions.assertNotNull(forum);
    }

    @Test
    void  ForumCanAddThread() {

        Forum forum = new Forum();

        User usr = new User("usr", new UserRoleScrummaster());

        BacklogItem bli = new BacklogItem("blitem", "blitemdesc", usr );

        Thread thread = new Thread("Title", usr, "Content", bli);

        forum.addThread(thread);

        List<Thread> lst = forum.getAllThreads();

        Thread t = lst.get(0);


        Assertions.assertEquals(thread, t);
    }

    @Test
    void  ForumCanAddPostOnThread() {

        Forum forum = new Forum();

        User usr = new User("usr", new UserRoleScrummaster());

        BacklogItem bli = new BacklogItem("blitem", "blitemdesc", usr );

        Thread thread = new Thread("Title", usr, "Content", bli);


        Post post = new Post("test", usr );

        thread.addPost(post);



        forum.addThread(thread);




        List<Thread> lst = forum.getAllThreads();

        Thread t = lst.get(0);

        List <Post> posts = t.getAllPosts();

        Post p = posts.get(1);


        Assertions.assertEquals(post, p);
    }

    @Test
    void  ForumRemoveThread() {
        Forum forum = new Forum();

        User usr = new User("usr", new UserRoleScrummaster());

        BacklogItem bli = new BacklogItem("blitem", "blitemdesc", usr );

        Thread thread = new Thread("Title", usr, "Content", bli);

        forum.addThread(thread);

        forum.removeThread(thread);

        List<Thread> lst = forum.getAllThreads();

        List<Thread> lst2 = new ArrayList<>();

        Assertions.assertEquals(lst, lst2);
    }

}
