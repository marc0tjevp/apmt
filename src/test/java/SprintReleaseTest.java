import domain.projectmangement.SprintRelease;
import domain.projectmangement.sprintreleasestate.SprintReleaseNotReleasedState;
import domain.projectmangement.sprintreleasestate.SprintReleaseReleasedState;
import domain.projectmangement.sprintreleasestate.SprintReleaseState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class SprintReleaseTest {
    @Test
    public void createSprintRelease(){
        SprintRelease rel = new SprintRelease();

        Assertions.assertNotNull(rel);
    }

    @Test
    public void getSetReleaseState(){
        SprintRelease rel = new SprintRelease();

        SprintReleaseState state = new SprintReleaseReleasedState();

        rel.setState(state);

        Assertions.assertEquals(rel.getState(), state);
    }

    @Test
    public void ReleaseTest(){
        SprintRelease rel = new SprintRelease();

        rel.release();
        boolean rels = false;

        if(!rel.getState().toString().contains("NotReleased") && !rel.getState().toString().contains("Canceled")){
            rels = true;}

        Assertions.assertTrue(rels);
    }

    @Test
    public void CancelTest(){
        SprintRelease rel = new SprintRelease();

        rel.cancel();
        boolean rels = false;

        if(!rel.getState().toString().contains("NotReleased") && rel.getState().toString().contains("Canceled")){
            rels = true;}

        Assertions.assertTrue(rels);
    }
}
