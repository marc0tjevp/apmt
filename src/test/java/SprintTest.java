import domain.projectmangement.BacklogItem;
import domain.projectmangement.Sprint;
import domain.projectmangement.sprintstate.SprintFinishedState;
import domain.user.User;
import domain.user.UserRoleStudent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDate.MAX;
import static java.time.LocalDate.MIN;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class SprintTest {

    @Test
    void createSprint(){
        Sprint s = new Sprint(MIN, MAX);

        Assertions.assertNotNull(s);
    }

    @Test
    void GetStartDate(){
        Sprint s = new Sprint(MIN, MAX);
        Assertions.assertEquals(MIN, s.getStartDate());
    }

    @Test
    void SetStartDate(){
        Sprint s = new Sprint(MIN, MAX);



       s.setStartDate(LocalDate.now());


        Assertions.assertEquals(LocalDate.now(), s.getStartDate());
    }

    @Test
    void GetEndDate(){
        Sprint s = new Sprint(MIN, MAX);
        Assertions.assertEquals(MAX, s.getEndDate());
    }

    @Test
    void SetEndDate(){
        Sprint s = new Sprint(MIN, MAX);



        s.setEndDate(LocalDate.now());


        Assertions.assertEquals(LocalDate.now(), s.getEndDate());
    }


    @Test
    void addGetBackLogItem(){
        User usr = new User("student", new UserRoleStudent());

        BacklogItem item = new BacklogItem("item", "desc", usr);

        Sprint s = new Sprint(MIN, MAX);
        s.addBacklogItem(item);

        List<BacklogItem> items = s.getAllBacklogItems();

        Assertions.assertEquals(item, items.get(0));
    }

    @Test
    void RemoveBackLogItem(){
        User usr = new User("student", new UserRoleStudent());

        BacklogItem item = new BacklogItem("item", "desc", usr);

        Sprint s = new Sprint(MIN, MAX);
        s.addBacklogItem(item);
        s.removeBacklogItem(item);
        List<BacklogItem> items = s.getAllBacklogItems();

        List<BacklogItem> items2 = new ArrayList<>();
        Assertions.assertEquals(items, items2);
    }

    @Test
    void GetSetState(){
        Sprint s = new Sprint(MIN, MAX);
        s.setState(new SprintFinishedState());

        boolean go = false;

        if(s.getState().toString().contains("Finished")){
            go = true;
        }

        Assertions.assertTrue(go);
    }


    @Test
    void Start(){
        Sprint s = new Sprint(MIN, MAX);
        s.start();

        boolean go = false;

        if(s.getState().toString().contains("OnGoing")){
            go = true;
        }

        Assertions.assertTrue(go);
    }

    @Test
    void SprintFinish(){
        Sprint s = new Sprint(MIN, MAX);
        s.start();
        s.finish();
        boolean go = false;

        if(s.getState().toString().contains("Finished")){
            go = true;
        }

        Assertions.assertTrue(go);
    }

    @Test
    void cantFinishNonStarted(){
        Sprint s = new Sprint(MIN, MAX);
        Exception a = null;
        try {
            s.finish();
        }catch (Exception e){
            a=e;
        }


        Assertions.assertNotNull(a);
    }

    @Test
    void cantStartFinished(){
        Sprint s = new Sprint(MIN, MAX);
        Exception a = null;
        s.start();
        s.finish();
        try {
            s.start();
        }catch (Exception e){
            a=e;
        }


        Assertions.assertNotNull(a);
    }

    @Test
    void cantStartStarted(){
        Sprint s = new Sprint(MIN, MAX);
        Exception a = null;
        s.start();

        try {
            s.start();
        }catch (Exception e){
            a=e;
        }


        Assertions.assertNotNull(a);
    }


}
