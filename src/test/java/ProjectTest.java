import domain.filemanagement.Repository;
import domain.forum.Forum;
import domain.projectmangement.Project;
import domain.projectmangement.Sprint;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDate.MAX;
import static java.time.LocalDate.MIN;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class ProjectTest {

    @Test
    void canCreate(){
        Project proj = new Project("New Project", "My first project");

        Assertions.assertNotNull(proj);
    }

    @Test
    void CanAddGetSprint(){
        Project proj = new Project("New Project", "My first project");
        Sprint s = new Sprint(MIN, MAX);
        proj.addSprint(s);

        Assertions.assertEquals(s, proj.getAllSprints().get(0));
    }

    @Test
    void CanRemoveSprint(){
        Project proj = new Project("New Project", "My first project");
        Sprint s = new Sprint(MIN, MAX);
        proj.addSprint(s);

        proj.removeSprint(s);

        List<Sprint> l1= proj.getAllSprints();

        List<Sprint> l2 = new ArrayList<>();

        Assertions.assertEquals(l1, l2);
    }

    @Test
    void getSetTitle(){
        Project proj = new Project("New Project", "My first project");

        proj.setTitle("1");

        Assertions.assertEquals("1", proj.getTitle());
    }

    @Test
    void getSetDesc(){
        Project proj = new Project("New Project", "My first project");

        proj.setDescription("1");

        Assertions.assertEquals("1", proj.getDescription());
    }

    @Test
    void getSetForum(){
        Project proj = new Project("New Project", "My first project");
        Forum forum = new Forum();
        proj.setForum(forum);

        Assertions.assertEquals(forum, proj.getForum());
    }

    @Test
    void getSetRepo(){
        Repository repo = new Repository();
        Project proj = new Project("New Project", "My first project");

        proj.setRepository(repo);

        Assertions.assertEquals(repo, proj.getRepository());
    }

    @Test
    void SetSprintList(){
        Project proj = new Project("New Project", "My first project");
        Sprint s = new Sprint(MIN, MAX);
        Sprint s2 = new Sprint(MIN, MAX);

        List<Sprint> lst = new ArrayList<>();
        lst.add(s);
        lst.add(s2);

        proj.setSprints(lst);


        List<Sprint> lst2 = proj.getAllSprints();

        Assertions.assertEquals(lst, lst2);
    }


    @Test
    void testToString(){
        Project proj = new Project("New Project", "My first project");

        String str = proj.toString();


        boolean go = false;

        if(str.contains("title=")){
            go = true;
        }

        Assertions.assertTrue(go);
    }


}
