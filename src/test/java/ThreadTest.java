import domain.forum.Post;
import domain.projectmangement.BacklogItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;


import domain.forum.Thread;


import domain.user.User;

import domain.user.UserRoleScrummaster;


import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class ThreadTest {

    @Test
    void  ThreadCanCreate(){


        User usr = new User("usr", new UserRoleScrummaster());

        BacklogItem bli = new BacklogItem("blitem", "blitemdesc", usr );

        Thread thread = new Thread("Title", usr, "Content", bli);

        Assertions.assertNotNull(thread);
    }

    @Test
    void  ThreadGetTitle(){


        User usr = new User("usr", new UserRoleScrummaster());

        BacklogItem bli = new BacklogItem("blitem", "blitemdesc", usr );

        Thread thread = new Thread("Title", usr, "Content", bli);

        String t = thread.getTitle();

        Assertions.assertEquals("Title", t);
    }

    @Test
    void  ThreadSetTitle(){


        User usr = new User("usr", new UserRoleScrummaster());

        BacklogItem bli = new BacklogItem("blitem", "blitemdesc", usr );

        Thread thread = new Thread("Title", usr, "Content", bli);

        thread.setTitle("abc");

        String t = thread.getTitle();

        Assertions.assertEquals("abc", t);
    }

    @Test
    void  ThreadGetSetInitialPost(){


        User usr = new User("usr", new UserRoleScrummaster());

        BacklogItem bli = new BacklogItem("blitem", "blitemdesc", usr );

        Thread thread = new Thread("Title", usr, "Content", bli);



        Post p = new Post("test", usr);

        thread.setInitialPost(p);

        Assertions.assertEquals(p, thread.getInitialPost());
    }

    @Test
    void  ThreadGetRelatedBackLogItem(){

        User usr = new User("usr", new UserRoleScrummaster());

        BacklogItem bli = new BacklogItem("blitem", "blitemdesc", usr );

        Thread thread = new Thread("Title", usr, "Content", bli);

        Assertions.assertEquals(bli, thread.getRelatedBacklogItem());
    }

    @Test
    void  ThreadSetRelatedBackLogItem(){

        User usr = new User("usr", new UserRoleScrummaster());

        BacklogItem bli = new BacklogItem("blitem", "blitemdesc", usr );

        Thread thread = new Thread("Title", usr, "Content", bli);

        BacklogItem bli2 = new BacklogItem("blitem2", "blitemdesc2", usr );

        thread.setRelatedBacklogItem(bli2);

        Assertions.assertEquals(bli2, thread.getRelatedBacklogItem());
    }


}
