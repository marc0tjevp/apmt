import domain.forum.Post;
import domain.projectmangement.BacklogItem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import domain.forum.Forum;
import domain.forum.Thread;
import domain.forum.Post;

import domain.user.User;
import domain.user.UserRole;
import domain.user.UserRoleStudent;
import domain.user.UserRoleProductOwner;
import domain.user.UserRoleScrummaster;

import domain.projectmangement.BacklogItem;
import org.junit.jupiter.api.TestTemplate;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class PostTest {

    @Test
    void PostCanCreate() {

        User usr = new User("student", new UserRoleStudent());
        Post pst = new Post("a", usr);
        Assertions.assertNotNull(pst);
    }

    @Test
    void PostGetOP() {

        User usr = new User("student", new UserRoleStudent());
        Post pst = new Post("a", usr);

        User op = pst.getOriginalPoster();

        Assertions.assertEquals(usr, op);
    }

    @Test
    void PostGetContent() {

        User usr = new User("student", new UserRoleStudent());
        Post pst = new Post("a", usr);

        String cont = pst.getContent();

        Assertions.assertEquals("a", cont );
    }

    @Test
    void PostSetContent() {

        User usr = new User("student", new UserRoleStudent());
        Post pst = new Post("a", usr);

        pst.setContent("b");

        String cont = pst.getContent();

        Assertions.assertEquals("b", cont);
    }

}
