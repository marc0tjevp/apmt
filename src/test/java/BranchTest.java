import domain.filemanagement.File;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import domain.filemanagement.Branch;

import java.util.ArrayList;
import java.util.List;

import static domain.filemanagement.FileType.FILE;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public class BranchTest {

    @Test
    void CreateBranch() {

        Branch branch = new Branch("test");

        Assertions.assertNotNull(branch);
    }

    @Test
    void GetSetBranchName() {

        Branch branch = new Branch("test");

        branch.setName("br");

        Assertions.assertEquals("br", branch.getName());
    }
    @Test
    void branchePushPull (){
        Branch branch = new Branch("test");

        File file = new File(FILE, "test.json", "0101010");
        File file2 = new File(FILE, "test.json", "0101010");

        List<File> files = new ArrayList<>();

        files.add(file);
        files.add(file2);

        branch.push(files);

        List<File> pfiles = branch.pull();

        Assertions.assertEquals(pfiles, files);


    }

//
    @Test
    void brancheGetSetFiles (){
        Branch branch = new Branch("test");

        File file = new File(FILE, "test.json", "0101010");
        File file2 = new File(FILE, "test.json", "0101010");

        List<File> files = new ArrayList<>();

        files.add(file);
        files.add(file2);

        branch.setFiles(files);

        List<File> pfiles = branch.getFiles();

        Assertions.assertEquals(pfiles, files);


    }

}

