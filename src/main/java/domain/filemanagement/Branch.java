package domain.filemanagement;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marco van Poortvliet
 * @version 1.0
 */
public class Branch {

    /**
     * The name of a Branch
     */
    private String name;

    /**
     * Collection of files in the Branch
     */
    private List<File> files;

    /**
     * Constructor for a Branch
     *
     * @param name The name of the Branch
     */
    public Branch(String name) {
        this.name = name;
        this.files = new ArrayList<>();
    }

    /**
     * Pull a list of files from the Branch
     */
    public List<File> pull() {
        return this.files;
    }

    /**
     * Push/commit a list of files from the Branch
     *
     * @param commitFiles Collection of Files
     */
    public void push(List<File> commitFiles) {
        this.files.addAll(commitFiles);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }
}
