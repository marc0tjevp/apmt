package domain.filemanagement;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class File {

    /**
     * The name of a File
     */
    private String name;

    /**
     * The content of a File
     */
    private String content;

    /**
     * FileType: File or folder
     */
    private FileType fileType;

    /**
     * Collection of Files within a Folder
     */
    private List<File> files;

    /**
     * A collection of comments on the File
     */
    private List<String> comments;

    /**
     * @param type    File or Folder
     * @param name    The name of the file or folder
     * @param content Content of the file, defaults to null for folders
     */
    public File(FileType fileType, String name, String content) {
        this.fileType = fileType;
        this.name = name;
        this.comments = new ArrayList<>();

        this.content = (fileType == FileType.FOLDER) ? null : content;
        this.files = (fileType == FileType.FOLDER) ? new ArrayList<>() : null;
    }

    /**
     * Return all files within folder
     */
    public List<File> getFiles() {
        return files;
    }

    /**
     * Add file or folder to folder
     *
     * @param file File object to add to a folder
     */
    public void addFile(File file) {
        this.files.add(file);
    }

    /**
     * Remove file or folder by object reference
     *
     * @param file File object to remove from a folder
     */
    public void removeFile(File file) {
        this.files.remove(file);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }
}
