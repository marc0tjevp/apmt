package domain.filemanagement;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marco van Poortvliet
 * @version 1.0
 */
public class Repository {

    /**
     * A collection of Branches
     */
    private List<Branch> branches;

    /**
     * Constructor for Repository
     */
    public Repository() {
        this.branches = new ArrayList<>();
    }

    /**
     * Add branch to Repository
     */
    public void addBranch(Branch branch) {
        this.branches.add(branch);
    }

    public List<Branch> getBranches(){
        return branches;
    }

    /**
     * Remove branch from Repository by object reference
     */
    public void removeBranch(Branch branch) {
        this.branches.remove(branch);
    }

}
