package domain.filemanagement;

public enum FileType {
    FOLDER,
    FILE
}
