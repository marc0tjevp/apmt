package domain.user;

public abstract class UserRoleFactory {
    public abstract UserRole createUserRole();
}
