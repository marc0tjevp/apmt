package domain.user;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class UserRoleProductOwner implements UserRole {
    @Override
    public String whoAmI() {
        return "Product Owner";
    }
}
