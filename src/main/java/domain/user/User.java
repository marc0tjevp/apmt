package domain.user;

/**
 * @author Marco van Poortvliet
 * @version 1.0
 */
public class User {

    /**
     * The username of a User, should be unique?
     */
    private String username;

    /**
     * The Role/Type of a User
     */
    private UserRole userRole;

    /**
     * Constructor for a User
     * @param username
     * @param userRole
     */
    public User(String username, UserRole userRole) {
        this.username = username;
        this.userRole = userRole;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
