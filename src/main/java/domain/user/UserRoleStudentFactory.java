package domain.user;

public class UserRoleStudentFactory extends UserRoleFactory {
    public UserRole createUserRole() {
        return new UserRoleStudent();
    }
}
