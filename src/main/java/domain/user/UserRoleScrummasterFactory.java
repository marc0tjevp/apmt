package domain.user;

public class UserRoleScrummasterFactory extends UserRoleFactory {
    public UserRole createUserRole() {
        return new UserRoleScrummaster();
    }
}
