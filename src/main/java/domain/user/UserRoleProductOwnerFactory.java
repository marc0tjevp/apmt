package domain.user;

public class UserRoleProductOwnerFactory extends UserRoleFactory {
    public UserRole createUserRole() {
        return new UserRoleProductOwner();
    }
}
