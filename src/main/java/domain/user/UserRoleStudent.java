package domain.user;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class UserRoleStudent implements UserRole {
    @Override
    public String whoAmI() {
        return "Student";
    }
}
