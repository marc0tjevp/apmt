package domain.user;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public interface UserRole {
    String whoAmI();
}
