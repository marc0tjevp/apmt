package domain.user;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class UserRoleScrummaster implements UserRole {
    @Override
    public String whoAmI() {
        return "Scrum Master";
    }
}
