package domain.forum;

import domain.user.User;

/**
 * @author Marco van Poortvliet
 * @version 1.0
 */
public class Post {

    /**
     * The Original Poster of a Post
     */
    private User originalPoster;

    /**
     * The content of a Post
     */
    private String content;

    /**
     * Constructor for a Post
     *
     * @param content The content of a Post
     */
    public Post(String content, User originalPoster) {
        this.content = content;
        this.originalPoster = originalPoster;
    }

    public User getOriginalPoster() {
        return originalPoster;
    }

    public void setOriginalPoster(User originalPoster) {
        this.originalPoster = originalPoster;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
