package domain.forum;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marco van Poortvliet
 * @version 1.0
 */
public class Forum {

    /**
     * A collection of threads
     */
    private List<Thread> threads;

    /**
     * Constructor for a Forum, which is created per Project
     */
    public Forum() {
        this.threads = new ArrayList<>();
    }

    /**
     * Adds a Thread to a Forum
     *
     * @param thread A Thread object
     */
    public void addThread(Thread thread) {
        this.threads.add(thread);
    }

    /**
     * Removes a Thread from a Forum by object reference
     *
     * @param thread A Thread object
     */
    public void removeThread(Thread thread) {
        this.threads.remove(thread);
    }

    /**
     * Returns all threads in a Forum
     *
     * @return ArrayList<Thread>
     */
    public List<Thread> getAllThreads() {
        return this.threads;
    }

}
