package domain.forum;

import domain.projectmangement.BacklogItem;
import domain.user.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marco van Poortvliet
 * @version 1.0
 */
public class Thread {

    /**
     * The title of a Thread
     */
    private String title;

    /**
     * The initial Post of a Thread
     */
    private Post initialPost;

    /**
     * A collection of posts, including the initial Post
     */
    private List<Post> posts;

    /**
     * A Thread is related to a BacklogItem
     */
    private BacklogItem relatedBacklogItem;

    /**
     * Constructor for a Thread
     *
     * @param title              The title of a Thread
     * @param content            The content for the initial Post of a Thread
     * @param relatedBacklogItem A related BacklogItem
     */
    public Thread(String title, User user, String content, BacklogItem relatedBacklogItem) {
        this.title = title;
        this.posts = new ArrayList<>();
        this.initialPost = new Post(content, user);
        this.relatedBacklogItem = relatedBacklogItem;

        // Add the initial Post
        this.posts.add(this.initialPost);
    }

    /*
     * Add a post to a thread to respond to the initial post
     */
    public void addPost(Post post){
        this.posts.add(post);
    }

    public List<Post> getAllPosts() {
        return posts;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Post getInitialPost() {
        return initialPost;
    }

    public void setInitialPost(Post initialPost) {
        this.initialPost = initialPost;
    }

    public BacklogItem getRelatedBacklogItem() {
        return relatedBacklogItem;
    }

    public void setRelatedBacklogItem(BacklogItem relatedBacklogItem) {
        this.relatedBacklogItem = relatedBacklogItem;
    }
}
