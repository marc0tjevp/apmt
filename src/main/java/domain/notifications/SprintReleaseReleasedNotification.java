package domain.notifications;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class SprintReleaseReleasedNotification extends Notification {

    @Override
    void create() {
        // Implement
    }

    @Override
    void send() {
        // Implement
    }
}
