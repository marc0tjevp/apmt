package domain.notifications;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public abstract class Notification {

    abstract void create();
    abstract void send();

    public final void sendNotification() {
        create();
        send();
    }

}
