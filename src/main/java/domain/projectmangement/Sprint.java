package domain.projectmangement;

import domain.projectmangement.sprintstate.SprintNotStartedState;
import domain.projectmangement.sprintstate.SprintState;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Marco van Poortvliet
 * @version 1.3
 */
public class Sprint implements SprintInterface {
    private LocalDate startDate;
    private LocalDate endDate;
    private List<BacklogItem> backlogItems;
    private SprintRelease sprintRelease;

    /**
     * Keep track of the SprintState, NotStarted, Ongoing or Finished
     */
    private SprintState state;

    /**
     * Constructor for a Sprint, which is listed under a Project
     *
     * @param startDate The starting date of the Sprint
     * @param endDate   The end date of the Sprint
     */
    public Sprint(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.backlogItems = new ArrayList<>();
        this.state = new SprintNotStartedState();
        this.sprintRelease = new SprintRelease();
    }


    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    /**
     * Adds a BacklogItem to a Sprint
     *
     * @param backlogItem A BacklogItem object
     */
    public void addBacklogItem(BacklogItem backlogItem) {
        this.backlogItems.add(backlogItem);
    }

    /**
     * Removes a BacklogItem from a Sprint by object reference
     *
     * @param backlogItem A BacklogItem object
     */
    public void removeBacklogItem(BacklogItem backlogItem) {
        this.backlogItems.remove(backlogItem);
    }

    /**
     * Returns all BacklogItems in a Sprint
     *
     * @return ArrayList<BacklogItem>
     */
    public List<BacklogItem> getAllBacklogItems() {
        return this.backlogItems;
    }

    @Override
    public void setState(SprintState state) {
        this.state = state;
    }

    @Override
    public SprintState getState(){
        return this.state;
    }

    @Override
    public void start() {
        this.state.start(this);
    }

    @Override
    public void finish() {
        this.state.finish(this);
    }

    public SprintRelease getSprintRelease() {
        return sprintRelease;
    }


}
