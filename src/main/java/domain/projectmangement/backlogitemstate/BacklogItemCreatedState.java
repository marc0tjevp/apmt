package domain.projectmangement.backlogitemstate;

import domain.projectmangement.BacklogItemInterface;

import domain.notifications.Notification;
import domain.notifications.BacklogItemToDoNotification;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class BacklogItemCreatedState implements BacklogItemState {

    @Override
    public void previous(BacklogItemInterface backlogItem) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void next(BacklogItemInterface backlogItem) {
        backlogItem.setState(new BacklogItemTodoState());
        Notification notify = new BacklogItemToDoNotification();
        notify.sendNotification();
    }
}
