package domain.projectmangement.backlogitemstate;

import domain.projectmangement.BacklogItemInterface;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class BacklogItemDoneState implements BacklogItemState {

    @Override
    public void previous(BacklogItemInterface backlogItem) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void next(BacklogItemInterface backlogItem) {
        throw new UnsupportedOperationException();
    }
}
