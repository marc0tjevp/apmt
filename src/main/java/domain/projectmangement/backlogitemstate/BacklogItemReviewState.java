package domain.projectmangement.backlogitemstate;

import domain.projectmangement.BacklogItemInterface;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class BacklogItemReviewState implements BacklogItemState {
    @Override
    public void previous(BacklogItemInterface backlogItem) {
        backlogItem.setState(new BacklogItemTodoState());
    }

    @Override
    public void next(BacklogItemInterface backlogItem) {
        backlogItem.setState(new BacklogItemDoneState());
    }
}
