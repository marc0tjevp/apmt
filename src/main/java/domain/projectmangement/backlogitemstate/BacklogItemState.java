package domain.projectmangement.backlogitemstate;

import domain.projectmangement.BacklogItemInterface;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public interface BacklogItemState {

    void previous(BacklogItemInterface backlogItem);

    void next(BacklogItemInterface backlogItem);
}
