package domain.projectmangement;

import domain.projectmangement.sprintstate.SprintState;

public interface SprintInterface {

    void setState(SprintState state);
    SprintState getState();
    void start();
    void finish();
}
