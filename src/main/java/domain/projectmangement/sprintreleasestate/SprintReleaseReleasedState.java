package domain.projectmangement.sprintreleasestate;

import domain.projectmangement.SprintReleaseInterface;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class SprintReleaseReleasedState implements SprintReleaseState {
    @Override
    public void release(SprintReleaseInterface sprintRelease) {
        throw new UnsupportedOperationException("Can not release a released sprint");
    }

    @Override
    public void cancel(SprintReleaseInterface sprintRelease) {
        throw new UnsupportedOperationException("Can not cancel a released sprint");
    }
}
