package domain.projectmangement.sprintreleasestate;

import domain.projectmangement.SprintReleaseInterface;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public interface SprintReleaseState {
    void release(SprintReleaseInterface sprintRelease);
    void cancel(SprintReleaseInterface sprintRelease);
}
