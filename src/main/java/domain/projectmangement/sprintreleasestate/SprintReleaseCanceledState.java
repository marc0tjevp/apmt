package domain.projectmangement.sprintreleasestate;

import domain.projectmangement.SprintReleaseInterface;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class SprintReleaseCanceledState implements SprintReleaseState {
    @Override
    public void release(SprintReleaseInterface sprintRelease) {
        sprintRelease.setState(new SprintReleaseReleasedState());
    }

    @Override
    public void cancel(SprintReleaseInterface sprintRelease) {
        throw new UnsupportedOperationException("Can not cancel a released release");
    }
}
