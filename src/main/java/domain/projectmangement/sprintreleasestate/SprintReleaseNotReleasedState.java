package domain.projectmangement.sprintreleasestate;

import domain.projectmangement.SprintReleaseInterface;
import domain.notifications.Notification;
import domain.notifications.SprintReleaseCanceledNotification;
import domain.notifications.SprintReleaseReleasedNotification;
/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class SprintReleaseNotReleasedState implements SprintReleaseState {
    @Override
    public void release(SprintReleaseInterface sprintRelease) {
        sprintRelease.setState(new SprintReleaseReleasedState());
        Notification notification = new SprintReleaseReleasedNotification();
        notification.sendNotification();
    }

    @Override
    public void cancel(SprintReleaseInterface sprintRelease) {
        sprintRelease.setState(new SprintReleaseCanceledState());
        Notification notification = new SprintReleaseCanceledNotification();
        notification.sendNotification();
    }
}
