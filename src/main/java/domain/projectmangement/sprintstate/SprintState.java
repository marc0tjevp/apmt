package domain.projectmangement.sprintstate;

import domain.projectmangement.SprintInterface;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public interface SprintState {

    void start(SprintInterface sprint);

    void finish(SprintInterface sprint);
}
