package domain.projectmangement.sprintstate;

import domain.projectmangement.SprintInterface;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class SprintFinishedState implements SprintState {
    @Override
    public void start(SprintInterface sprint) {
        throw new UnsupportedOperationException("Can not start a finished sprint");
    }

    @Override
    public void finish(SprintInterface sprint) {
        throw new UnsupportedOperationException("Sprint is already finished");
    }
}
