package domain.projectmangement.sprintstate;

import domain.projectmangement.SprintInterface;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class SprintNotStartedState implements SprintState {
    @Override
    public void start(SprintInterface sprint) {
        sprint.setState(new SprintOnGoingState());
    }

    @Override
    public void finish(SprintInterface sprint) {
        throw new UnsupportedOperationException("Can only finish an ongoing sprint");
    }
}
