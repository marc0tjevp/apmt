package domain.projectmangement;

import domain.projectmangement.sprintreleasestate.SprintReleaseState;

public interface SprintReleaseInterface {
    void setState(SprintReleaseState state);
    SprintReleaseState getState();
    void release();
    void cancel();
}
