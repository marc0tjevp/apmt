package domain.projectmangement;

import domain.filemanagement.Repository;
import domain.forum.Forum;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Marco van Poortvliet
 * @version 1.3
 */
public class Project {
    private String title;
    private String description;
    private List<Sprint> sprints;
    private Forum forum;
    private Repository repository;

    /**
     * Constructor for a Project
     *
     * @param title       The title of a Project
     * @param description A description for a Project
     */
    public Project(String title, String description) {
        this.title = title;
        this.description = description;
        this.sprints = new ArrayList<>();
        this.forum = new Forum();
        this.repository = new Repository();
    }

    /**
     * Adds a Sprint to a Project
     *
     * @param sprint A Sprint object
     */
    public void addSprint(Sprint sprint) {
        this.sprints.add(sprint);
    }

    /**
     * Removes a Sprint from a Project by object reference
     *
     * @param sprint A Sprint object
     */
    public void removeSprint(Sprint sprint) {
        this.sprints.remove(sprint);
    }

    /**
     * Returns all sprints in a Project
     *
     * @return ArrayList<Sprint>
     */
    public List<Sprint> getAllSprints() {
        return this.sprints;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSprints(List<Sprint> sprints) {
        this.sprints = sprints;
    }

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    @Override
    public String toString() {
        return "Project{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", sprints=" + sprints +
                ", forum=" + forum +
                '}';
    }
}
