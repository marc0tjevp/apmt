package domain.projectmangement;

import domain.projectmangement.backlogitemstate.BacklogItemState;

public interface BacklogItemInterface {

    void setState(BacklogItemState state);

    BacklogItemState getState();

    void previous();

    void next();
}
