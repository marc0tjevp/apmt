package domain.projectmangement;

import domain.projectmangement.backlogitemstate.BacklogItemCreatedState;
import domain.projectmangement.backlogitemstate.BacklogItemState;
import domain.user.User;

/**
 * @author Marco van Poortvliet
 * @version 1.1
 */
public class BacklogItem implements BacklogItemInterface {

    /**
     * The title of a BacklogItem, possibly a User Story
     */
    private String title;

    /**
     * The description of a BacklogItem
     */
    private String description;

    /**
     * The state of a BacklogItem
     */
    private BacklogItemState state;

    //assigned user
    private User assignedUser;

    /**
     * Constructor for a BacklogItem, which is stored in a Sprint
     *
     * @param title       The title of the item/issue
     * @param description A description for this item/issue
     */
    public BacklogItem(String title, String description, User assignedUser) {
        this.title = title;
        this.description = description;
        this.state = new BacklogItemCreatedState();
        this.assignedUser = assignedUser;
    }

    /**
     * Returns the title of a BacklogItem
     *
     * @return String title
     */
    public String getTitle() {
        return title;
    }


    //
    public User getAssignedUser() {
        return assignedUser;
    }
    public void setAssignedUser( User assignedUser) {
        this.assignedUser = assignedUser;
    }

    /**
     * Set the title of a BacklogItem
     *
     * @param title The title of a BacklogItem
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the description of a BacklogItem
     *
     * @return String description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the description of a BacklogItem
     *
     * @param description The description of a BacklogItem
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void setState(BacklogItemState state) {
        this.state = state;
    }

    @Override
    public BacklogItemState getState() {
        return this.state;
    }

    @Override
    public void previous() {
        this.state.previous(this);
    }

    @Override
    public void next() {
        this.state.next(this);
    }
}
