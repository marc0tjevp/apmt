package domain.projectmangement;

import domain.projectmangement.sprintreleasestate.SprintReleaseNotReleasedState;
import domain.projectmangement.sprintreleasestate.SprintReleaseState;

public class SprintRelease implements SprintReleaseInterface {

    /**
     * Keep track of the SprintReleaseState
     */
    private SprintReleaseState state;

    public SprintRelease() {
        this.state = new SprintReleaseNotReleasedState();
    }

    @Override
    public void setState(SprintReleaseState state) {
        this.state = state;
    }

    @Override
    public SprintReleaseState getState() {
        return this.state;
    }

    @Override
    public void release() {
        this.state.release(this);
    }

    @Override
    public void cancel() {
        this.state.cancel(this);
    }
}
